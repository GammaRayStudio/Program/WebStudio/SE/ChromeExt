$(function () {
  var $btnAction = $("#btnAction");
  var $console = $("#console");
  $btnAction.off("click").on("click", function () {
    $console.text("OwO / Hello.");
    console.log("OwO / Hello.");
  });

  // 旋轉畫面 (clone)
  var getSelectedTab = (tab) => {
    var tabId = tab.id;
    var sendMessage = (messageObj) =>
      chrome.tabs.sendMessage(tabId, messageObj);
    document
      .getElementById("rotate")
      .addEventListener("click", () => sendMessage({ action: "ROTATE" }));
    document
      .getElementById("reset")
      .addEventListener("click", () => sendMessage({ action: "RESET" }));
  };
  chrome.tabs.getSelected(null, getSelectedTab);


});
