Chrome 擴充功能 , 研究測試
======


demo
------
`chrome://extensions`

+ 載入未封裝項目
+ 最重要的為 manifest.json 檔案，用來配置 Chreom 擴充應用的資訊

**測試**
+ button => Action
    + msg : `OwO / Hello.`
+ button => 翻轉、還原
    + <https://www.google.com.tw/>
    + <https://gamma-ray-studio.github.io/>


已安裝的 Chrome Extension 路徑 
------
```
~/Library/Application\ Support/Google/Chrome/Default/Extensions
```

參考資料
------
### 簡單來做一個 chrome extension
<https://medium.com/hybrid-maker/簡單來做一個-chrome-extension-2359e43f282a>

### Chrome外掛(擴充套件)開發全攻略(完整demo)
+ <https://www.796t.com/article.php?id=284177>
+ <https://github.com/sxei/chrome-plugin-demo>

### [Chrome extensions]第一個APP上架體驗
<https://stanleytw.com/chrome-extensions-first-app/>

### Chrome外掛開發入門
<https://iter01.com/595866.html>

### Chrome Extension 實戰經驗分享，從發想、實作、上架到萬人下載
<https://tw.alphacamp.co/blog/how-to-make-a-chrome-extension>

